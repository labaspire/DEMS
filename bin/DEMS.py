#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# run with python3 DEMS.py 2> /dev/null
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

████████▄     ▄████████   ▄▄▄▄███▄▄▄▄      ▄████████ 
███   ▀███   ███    ███ ▄██▀▀▀███▀▀▀██▄   ███    ███ 
███    ███   ███    █▀  ███   ███   ███   ███    █▀  
███    ███  ▄███▄▄▄     ███   ███   ███   ███        
███    ███ ▀▀███▀▀▀     ███   ███   ███ ▀███████████ 
███    ███   ███    █▄  ███   ███   ███          ███ 
███   ▄███   ███    ███ ███   ███   ███    ▄█    ███ 
████████▀    ██████████  ▀█   ███   █▀   ▄████████▀  
                                                     

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
DEMS - Datalogging and Energy Management System  v1.0
Developed by : Dogan BASARAN
               bsrndogan@gmail.com
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

from PyQt5.QtGui import *
from PyQt5 import QtCore, QtGui, QtWidgets
import os, sys
import _thread
import datetime
from functools import partial
import pickle as cPickle
import pickle
import rrdtool
from multiprocessing import Process
from configparser import SafeConfigParser
os.chdir("../")
sys.path.append('lib')
from a_ekle import *
from a_kaldir import *
from veri_dinle import *
from kontrol_ekrani import *
from html_update import*
os.chdir("bin")


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        
        #create main window actions
        self.createActions()
        
        #crate main window 
        self.createMenus()
        
        #define process events object
        QtWidgets.QApplication.processEvents()
        
        #set meain window icon
        self.setWindowIcon(QtGui.QIcon(os.getcwd()+"/../data/icon/DEMSLOGO.png"))
        
        # set main window title
        self.setWindowTitle("DEMS - Datalogging and Energy Management System")
        
        #define main window minimun size ( px )
        self.setMinimumSize(545,300)
        #self.setMaximumSize(545,300)
        
        #defined for statu and other leds 
        self.degis = True
        
        #define main window refresh function
        self.refresh()
        
        #call refresh function with timer
        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(600) #trigger every minute.

    def refresh(self):
    
        #Read config file for alert limits
        config = SafeConfigParser()
        config.read("../conf/dems.cfg")
        config.sections()
        
        
        #read saved unit names and unit hostnames
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
        pilist=cPickle.load(datapi)
        datapi.close()
        
        #define main window widget
        widget = QtWidgets.QWidget()
        
        #set center main widget
        self.setCentralWidget(widget)
        
        #define grid layout
        vbox = QtWidgets.QGridLayout()
        

        #define general fons
        font_other = QtGui.QFont("TimesNewRoman",7,QtGui.QFont.Normal,False)
        
        #define unit name fonts
        font_konum = QtGui.QFont("TimesNewRoman",14,QtGui.QFont.Normal,False)
        
        #define button text font
        font_but = QtGui.QFont("TimesNewRoman",12,QtGui.QFont.Normal,False)
        
        #define location dict
        konum={}
        
        #define button dict
        buton={}
        
        #define unit state dict
        birim_durum = {}
        
        #define led image dict
        led={}
        
        #define bulb image dict
        bulb={}
        
        #define air conditioner dict
        klima={}
        
        #define active user state 
        kullanici = {}
        
        #define active user image
        man = {}
        
        #define air conditioner text
        klima_bilgisi={}
        
        #define bulb etc
        aydinlatma={}
        
        #define bulb etc
        aydinlatma_bilgisi={}
        
        
        #define empty labels
        bosluk1=QtWidgets.QLabel()
        bosluk2=QtWidgets.QLabel()
        bosluk3=QtWidgets.QLabel()
        bosluk4=QtWidgets.QLabel()
        bosluk5=QtWidgets.QLabel()
        
        #define start row 
        satir_no=3
        
        #load red led
        led_kirmizi=QtWidgets.QLabel()
        led_kirmizi.setPixmap(QPixmap(os.getcwd()+"/../data/icon/rd.png"))
        
        #load blue led
        led_mavi=QtWidgets.QLabel()
        led_mavi.setPixmap(QPixmap(os.getcwd()+"/../data/icon/blue.png"))
        
        #load green led
        led_yesil=QtWidgets.QLabel()
        led_yesil.setPixmap(QPixmap(os.getcwd()+"/../data/icon/gr.png"))
        
        #settings of red led
        bilgi_kirmizi=QtWidgets.QLabel()
        bilgi_kirmizi.setText("Acil Durum")
        bilgi_kirmizi.setStyleSheet('color: dimgrey')
        bilgi_kirmizi.setFont(font_other)
        
        #settings of blue leds
        bilgi_mavi=QtWidgets.QLabel()
        bilgi_mavi.setText("Veri Alınamıyor")
        bilgi_mavi.setStyleSheet('color: dimgrey')
        bilgi_mavi.setFont(font_other)
        
        #setting of green leds
        bilgi_yesil=QtWidgets.QLabel()
        bilgi_yesil.setText("Kontrol Ediliyor")
        bilgi_yesil.setStyleSheet('color: dimgrey')
        bilgi_yesil.setFont(font_other)
        
        #selected unit names and unit hostnames loop
        #every unit interface starting with this loop
        for self.piname,self.hostname in sorted(pilist.items()):
            piname = self.piname
            hostname = self.hostname



            #Konfigurasyon dosyasından belirlenen min-max uyarı degerleri alınıyor.
            try :
                MinTemp = int(config.get(piname,"MinTemp"))
                #print(MinTemp)
            except :
                MinTemp = int(config.get("DEFAULT","MinTemp"))
                
            try :
                MaxTemp = int(config.get(piname,"MaxTemp"))
                #print(MinTemp)
            except :
                MaxTemp = int(config.get("DEFAULT","MaxTemp"))                
            
            try :
                MinHum = int(config.get(piname,"MinHum"))
                #print(MinTemp)
            except :
                MinHum = int(config.get("DEFAULT","MinHum"))            
            
            try :
                MaxHum = int(config.get(piname,"MaxHum"))
                #print(MinTemp)
            except :
                MaxHum = int(config.get("DEFAULT","MaxHum"))            

            try :
                MaxAQI = int(config.get(piname,"MaxAQI"))
                #print(MinTemp)
            except :
                MaxAQI = int(config.get("DEFAULT","MaxAQI")) 
                
                
                
            try:    
                #Secili alanın en guncel verileri alınıyor.    
                sonVeriler=rrdtool.lastupdate(os.getcwd()+"/../data/"+self.hostname+".rrd")
                sonGuncellemezamani=sonVeriler['date']
                sonSensorVerisi=sonVeriler['ds']
                
                sicaklik=sonVeriler['ds']['sicaklik']
                nemi=sonVeriler['ds']['nemorani']
                isiki=sonVeriler['ds']['isik']
                havak=sonVeriler['ds']['havakalitesi']                
            except:
                print(">>"+piname+" Unit db just not created")
                pass
            
            
                    
                               
            #I dont like "while True:" loop :) 
            if 1 > 0:
            
                #grif layout defined
                grid=QtWidgets.QGridLayout()

                
                #creating selected unit's name label
                konum[piname]=QtWidgets.QLabel()
                konum[piname].setStyleSheet('color: teal')
                konum[piname].setFont(font_konum)
                
                #creating selected unit's button
                buton[piname]=QtWidgets.QPushButton()
                buton[piname].setStyleSheet('color: teal')
                buton[piname].setFont(font_but)
                
                #creating selected unit's unit status led
                birim_durum[piname]=QtWidgets.QLabel()
                led[piname]=QtWidgets.QLabel()

                #creating leds informations label
                ledler=QtWidgets.QLabel()
                
                #ledler.setPixmap(QPixmap(os.getcwd()+"/../data/icon/ledler.png"))

                #creatin air cond. label
                klima[piname]=QtWidgets.QLabel()
                
                #creating bulb status label
                bulb[piname] = QtWidgets.QLabel()
                
                #creating user status label
                kullanici[piname]=QtWidgets.QLabel()
                
                #creating man icon label
                man[piname] = QtWidgets.QLabel()
                
                #klima_bilgisi[piname]=QLabel()
                
                #creating bulb information label
                aydinlatma[piname]=QtWidgets.QLabel()
                
                
                #aydinlatma_bilgisi[piname]=QLabel()
                
                #trying to get last update times for unit status led color 
                try:
                    son_guncelleme=rrdtool.lastupdate(os.getcwd()+"/../data/"+hostname+".rrd")
                    son_guncelleme_zaman=son_guncelleme["date"]
                    anlik_zaman=datetime.datetime.now()
                    
                    #if last update time bigger than 2 minute and change = True; unit statu led color = blue
                    if (son_guncelleme_zaman+datetime.timedelta(minutes=2))<anlik_zaman and self.degis == True:
                        image="blue.png"
                        
                    #if last update time bigger than 2 minute and change = False; unit statu led color = gray
                    elif(son_guncelleme_zaman+datetime.timedelta(minutes=2))<anlik_zaman and self.degis == False:
                        image="off.png"
                        
                    #if last update time smaller than 2 minute and change = True; check last unit data and select led color
                    elif(son_guncelleme_zaman+datetime.timedelta(minutes=2))>anlik_zaman and self.degis == True:
                        #get last unit data
                        veri_on_dakika=rrdtool.fetch(os.getcwd()+"/../data/"+hostname+".rrd","AVERAGE","-r","600","-s",str(int(time.time()-600)),"-e",str(int(time.time()-0)))
                        sensor_verileri=veri_on_dakika[2]
                        hava_kalitesi=sensor_verileri[0][2]
                        
                        #check last datas and select led color
                        try:
                            #check air quality, temperature and humidity data. if datas different at config's datas led color = red
                            #else : led color = green
                            if hava_kalitesi > MaxAQI or sicaklik < MinTemp or sicaklik > MaxTemp or nemi < MinHum or nemi > MaxHum:
                                image="rd.png"
                            else:
                                image="gr.png"
                        except:
                            image="gr.png"
                    
                    #if last update time smaller than 2 minute and change = False; unit statu led color = gray
                    elif(son_guncelleme_zaman+datetime.timedelta(minutes=2))>anlik_zaman and self.degis == False:
                        image="off.png"
                    else:
                        pass

                    #get last energy usage and motion datas
                    sonVeriler=rrdtool.lastupdate(os.getcwd()+"/../data/"+self.hostname+"enerji.rrd")

                    #get recorded data types
                    sonSensorVerisi=sonVeriler['ds']
                    
                    #get last energy usage data
                    tuketim=sonVeriler['ds']['tuketim']
                    
                    #get last motion data
                    hareket=sonVeriler['ds']['hareket']
                    
                                        
                    #check database last update time
                    #if db last updates time delta < 2; select energy and motion status
                    if (son_guncelleme_zaman+datetime.timedelta(minutes=2)) > anlik_zaman:
                    
                        #if last energy usage bigger than 0.5; bulb icon is yellow
                        if tuketim > 0.5:
                            if self.degis == True:
                                bulbstate = "Açık  "
                                imagebulb = "onnnd.png" #maybe its off 
                            elif self.degis == False:
                                bulbstate = "Açık  "
                                imagebulb = "onnnd.png" #maybe its off      
                                
                        #else bulb icon is gray                        
                        else:
                            bulbstate = "Kapalı"
                            imagebulb = "offfd.png" #maybe its off  
                            
                            
                        #if last motion data > 0.2; motion icon is orange and running                           
                        if hareket > 0.2:
                            if self.degis == True:
                                manstate = "Aktif  "
                                imageman = "aktif.png" #maybe its off 
                            elif self.degis == False:
                                manstate = "Aktif"
                                imageman = "paktif.png" #orange pasif  
                                    
                        #else motion icon is gray
                        else:
                            manstate = "Pasif"
                            imageman = "pasif.png" #maybe its off 

                    ##if db last updates time delta > 2; energy and motion status not known
                    else :
                        imageman = "pasif.png"
                        imagebulb = "offfd.png"

                #if detecting error report it log file    
                except:
                    imageman = "pasif.png"
                    imagebulb = "offfd.png"
                    image = "off.png"
                
                #load led icon
                led[piname].setPixmap(QPixmap(os.getcwd()+"/../data/icon/"+image))
                
                #load bulb icon
                bulb[piname].setPixmap(QPixmap(os.getcwd()+"/../data/icon/"+imagebulb))
                
                #load man icon
                man[piname].setPixmap(QPixmap(os.getcwd()+"/../data/icon/"+imageman))
                
                #set unit button text
                buton[piname].setText("  Detaylar  ")
                
                #set unit button geometry
                buton[piname].setGeometry(10,10,10,10)
                
                #define button connection
                buton[piname].clicked.connect(partial(self.kont_monit,piname,hostname))
                
                #define unit text
                konum[piname].setText(" "+str(piname)+"  ")
                
                #set unit statu text
                birim_durum[piname].setText(" Birim Durumu :")
                birim_durum[piname].setStyleSheet('color: dimgray')
                birim_durum[piname].setFont(font_but)    
                
                #set user statu text
                kullanici[piname].setText(" Kullanıcı :")
                kullanici[piname].setStyleSheet('color: dimgray')
                kullanici[piname].setFont(font_but)     
                        
                #set bulb statu text
                klima[piname].setText(" Aydınlatma :")
                klima[piname].setStyleSheet('color: dimgray')
                klima[piname].setFont(font_but)
                
                # maybe its neccesary
                aydinlatma[piname].setText("")
                aydinlatma[piname].setStyleSheet('color: dimgray')
                aydinlatma[piname].setFont(font_but)
                
                #add everything on layout
                vbox.addWidget(konum[piname],satir_no,0)
                vbox.addWidget(buton[piname],satir_no,1)
                vbox.addWidget(birim_durum[piname],satir_no,2)
                vbox.addWidget(led[piname],satir_no,3)
                vbox.addWidget(klima[piname],satir_no,4)
                vbox.addWidget(bulb[piname],satir_no,5)
                vbox.addWidget(kullanici[piname],satir_no,7)
                vbox.addWidget(man[piname],satir_no,8)
                #vbox.addWidget(aydinlatma[piname],satir_no,5)
                vbox.setAlignment(Qt.AlignTop)
                
                #add layout to central widget
                widget.setLayout(vbox)
                
                #define new unit row number
                satir_no=satir_no+1
            else:
                pass
        kayitli_alan_sayisi=QtWidgets.QLabel()
        kayitli_alan_sayisi.setText(str(len(pilist))+" alan kayıtlı")
        kayitli_alan_sayisi.setStyleSheet('color: teal')
        kayitli_alan_sayisi.setFont(font_other)
        
        vbox.addWidget(bosluk1,satir_no,0)
        vbox.addWidget(bosluk2,satir_no+1,0)
        vbox.addWidget(bosluk3,satir_no+2,0)
        vbox.addWidget(bosluk4,satir_no+3,0)
        #vbox.addWidget(bosluk5,satir_no+4,0)

        satir_no=satir_no+5
        
        vbox.addWidget(kayitli_alan_sayisi,satir_no+6,0)
        vbox.addWidget(led_yesil,satir_no+4,8)
        vbox.addWidget(bilgi_yesil,satir_no+4,7)
        vbox.addWidget(led_kirmizi,satir_no+5,8)
        vbox.addWidget(bilgi_kirmizi,satir_no+5,7)
        vbox.addWidget(led_mavi,satir_no+6,8)
        vbox.addWidget(bilgi_mavi,satir_no+6,7)
        #vbox.addWidget(ledler,satir_no+7,0,0,3)
        logo=QtWidgets.QLabel()
        logo.setPixmap(QPixmap(os.getcwd()+"/../data/icon/DEMSLOGOmain.png"))
        vbox.addWidget(logo,0,0,1,9)
        widget.setLayout(vbox)
        widget.show()
        
        if self.degis:
            self.degis = False
        else:
            self.degis = True

        for self.piname,self.hostname in sorted(pilist.items()):
            piname = self.piname
            hostname = self.hostname
            if os.path.isfile(os.getcwd()+"/../dems/"+piname+".html"):
                pass
            else:
                #go to dems directory
                os.chdir("../dems/")
                
                #generate unit web page source
                os.system("cp template.html "+piname+".html")
                
                #edit units web pages
                update_html(piname,hostname,pilist)
                edit_html(piname,hostname)
                
                #update remote host
                try:
                    #send main page
                    os.system("scp main.html nkiwz8o19vf0@labaspire.com:/home/nkiwz8o19vf0/public_html/dems/")
                    print("main window up to date")
                    
                    #send unit page
                    os.system("scp "+piname+".html nkiwz8o19vf0@labaspire.com:/home/nkiwz8o19vf0/public_html/dems/")
                    print("unit window up to date")
                    
                except:
                    print("New unit file could not created - Centeral unit network error")
                os.chdir("../data/")

	        


    def kont_monit(self,piname,hostname):
        #get selected unit name 
        self.piname = piname
        
        #get selected hostname
        self.hostname = hostname
        
        #define selected unit dict
        secili_alan={}
        secili_alan[piname]=hostname
        
        #bunlar biyere yazilmali ve kontrol ekrani kimliği için kullanılmalı
        if os.path.isfile(os.getcwd()+"/../data/secili_alan.db"):
            selected = open(os.getcwd()+"/../data/secili_alan.db", 'wb')
            pickle.dump(secili_alan, selected)
            selected.close()
            #print(secili_alan)

        else:
            selected = open(os.getcwd()+"/../data/secili_alan.db", 'wb')
            pickle.dump(secili_alan, selected)
            selected.close()
        #time.sleep(1)
        self.dialog=alan_ekran(self)
        self.dialog.show()

    #define monitor
    def monitor(self):
        window.show()
    #define about screen
    def about(self):
        QtWidgets.QMessageBox.about(self, "Program Hakkında","DEMS'in geliştirilme amacı; tek merkezden yönetilen tek birimli binalarda ortam kalitesi merkeze raporlamak ve aydınlatma cihazlarının otomatik kontrolünü sağlamaktır.")
    
    #define other about 
    def aboutQt(self):
        QtWidgets.QMessageBox.about(self, "AspireLab Hakkında","AspireLab Recep Tayyip Erdoğan Üniversitesi Mühendislik Fakültesinde bulunan Uygulamalı Sinyal İşleme ve Ensturmantasyon Laboratuvarıdır.")
        
    #add unit
    def ekle(self):
        self.dialog=alan_ekleme(self)
        self.dialog.show()

    #remove unit
    def kaldir(self):
        self.dialog=alan_kaldirma(self)
        self.dialog.show()
    
    #close program
    def close(self):
        veri_akisi.terminate()

    def createActions(self):
        self.monitorAct = QtWidgets.QAction("Kontrol Monitörü",self,shortcut=QtGui.QKeySequence.New,statusTip="Monitör Ekranını Göster", triggered=self.monitor)

        self.exitAct = QtWidgets.QAction("Çıkış", self, shortcut="Ctrl+Q",
                statusTip="Programdan çık", triggered=self.close)

        self.aboutAct = QtWidgets.QAction("Program Hakkında", self,
                statusTip="Program hakkında bilgi verir",
                triggered=self.about)

        self.aboutQtAct = QtWidgets.QAction("AspireLab Hakkında", self,
                statusTip="AspireLab hakkında bilgi verir",
                triggered=self.aboutQt)
        self.ekleAct = QtWidgets.QAction("Uç Birim Ekle", self,
                shortcut=QtGui.QKeySequence.New,
                statusTip="Uç birim ekle.", triggered=self.ekle)
        self.kaldirAct = QtWidgets.QAction("Uç Birim Kaldır", self,
                shortcut=QtGui.QKeySequence.New,
                statusTip="Uç birim kaldır.", triggered=self.kaldir)

    def createMenus(self):
        #define fonts
        self.font_but = QtGui.QFont("Monospace",4,QtGui.QFont.Normal,False)
        
        #define menubar
        self.menumenu=self.menuBar().addMenu("Menü")
        self.menumenu.addSeparator()
        self.menumenu.addAction(self.monitorAct)
        self.menumenu.addAction(self.exitAct)
    
        #define settings menu
        self.editMenu = self.menuBar().addMenu("Ayarlar")
        self.editMenu.addAction(self.ekleAct)
        self.editMenu.addAction(self.kaldirAct)
        
        #define help menu
        self.helpMenu = self.menuBar().addMenu("&Yardim")
        self.helpMenu.addAction(self.aboutAct)
        self.helpMenu.addAction(self.aboutQtAct)
        
        #define menubar beckground
        self.setStyleSheet("""QMenuBar {background-color: gray; color : white;}""")
        #self.setPointSize(9)
        self.setFont(self.font_but)


#RUN
if __name__ == '__main__':

    #checking unit name and hostnames database
    if os.path.isfile(os.getcwd()+"/../data/veritabani.db"):
        #if database already exist = pass
        pass
    #if database not exist create veritabani.db
    else:
        pilist={}
        pickle.dump(pilist, open(os.getcwd()+"/../data/veritabani.db", "wb" ) )
        print("Units database generated")
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'wb')
        pickle.dump(pilist, datapi)
        datapi.close()


    #Start mqtt subscriber and get data to all units
    veri_akisi=Process(target=server)
    veri_akisi.start()
    
    #wait one second for file operations
    time.sleep(1)
    
    #define Qt application
    app = QtWidgets.QApplication(sys.argv)
    
    #define application font
    app.setFont(QFont("TimesNewRoman", 7))
    
    #define application event process
    app.processEvents()
    
    #call MainWindow class
    window = MainWindow()
    
    #show main window
    window.show()
    sys.exit(app.exec_())

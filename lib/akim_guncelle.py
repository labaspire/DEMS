#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rrdtool
from rrdtool import update as rrd_update
import time
import datetime
import os

def TuketimHareketVeritabani(dosyaKonumu):
    veritabani=rrdtool.create(dosyaKonumu,
    "--step","20","--start",'0',
    "DS:tuketim:GAUGE:600:0:2",
    "DS:hareket:GAUGE:600:0:1",
    "RRA:AVERAGE:0.5:1:4320",
    "RRA:AVERAGE:0.5:6:10080",
    "RRA:AVERAGE:0.5:24:11160",
    "RRA:AVERAGE:0.5:288:22200",
    "RRA:MAX:0.5:1:4320",
    "RRA:MAX:0.5:6:10080",
    "RRA:MAX:0.5:24:11160",
    "RRA:MAX:0.5:288:22200",
    "RRA:MIN:0.5:1:4320",
    "RRA:MIN:0.5:6:10080",
    "RRA:MIN:0.5:24:11160",
    "RRA:MIN:0.5:288:22200")
    return veritabani

    
def TuketimHareketVeritabaniGuncelle(dosyaKonumu,tuketim,hareket,dosyaAdi):
   
    rrd_update(dosyaKonumu,'N:%s:%s'%(tuketim,hareket)); 



def TuketimHareketGrafik(piid,dosyaKonumu):


    piid=str(piid)
    sonGuncelleme = rrdtool.last(dosyaKonumu)
    sonGuncellemedf = datetime.datetime.fromtimestamp(int(sonGuncelleme)).strftime('%Y-%m-%d %H\:%M\:%S')
    for z in ["daily","weekly","monthly","yearly"]:
        if z== "daily":
            period="d"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"günlükEnerji.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hareket ve Tuketim", "--height=230",
            "DEF:tuketim="+dosyaKonumu+":tuketim:MAX",
            "DEF:hareket="+dosyaKonumu+":hareket:MAX",
            "AREA:tuketim#066e009b:Tuketim",
            "AREA:hareket#001c407d:Hareket",
            "COMMENT:\\n",            
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf) 
        if z== "weekly":
            period="w"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"haftalikEnerji.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hareket ve Tuketim", "--height=230",
            "DEF:tuketim="+dosyaKonumu+":tuketim:MAX",
            "DEF:hareket="+dosyaKonumu+":hareket:MAX",
            "AREA:tuketim#066e009b:Tuketim",
            "AREA:hareket#001c407d:Hareket",
            "COMMENT:\\n",            
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf) 
        if z== "monthly":
            period="m"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"aylikEnerji.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hareket ve Tuketim", "--height=230",
            "DEF:tuketim="+dosyaKonumu+":tuketim:MAX",
            "DEF:hareket="+dosyaKonumu+":hareket:MAX",
            "AREA:tuketim#066e009b:Tuketim",
            "AREA:hareket#001c407d:Hareket",
            "COMMENT:\\n",            
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf) 
        if z== "yearly":
            period="y"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"yillikEnerji.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hareket ve Tuketim", "--height=230",
            "DEF:tuketim="+dosyaKonumu+":tuketim:MAX",
            "DEF:hareket="+dosyaKonumu+":hareket:MAX",
            "AREA:tuketim#066e009b:Tuketim",
            "AREA:hareket#001c407d:Hareket",
            "COMMENT:\\n",            
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)        
            
            
            
            
            

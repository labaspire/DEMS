#!/usr/bin/python3
import paho.mqtt.subscribe as subscribe
import _thread, threading
import time
from functools import partial
import os
import pickle as cPickle
from veritabani_guncelle import *
geldi = 999



def on_message_print(client, userdata, message):
    zaman=time.strftime("%H%M%S")
    gelen=message.payload.decode()
    veri=gelen.split(",")
    
    #print(veri)
    
    Host = veri[0]
    try:
        Temp = float(veri[1]) #in temp 1
    except:
        Temp = float(23)
    try:
        Hum = float(veri[2])
    except:
        Hum = float(50)
    AQI = float(veri[3])
    Light = float(veri[4])
    Motion = float(veri[5])
    OutTemp = float(veri[6])
    Amp = float(veri[7])
    
    Temp2 = float(veri[8]) #in temp 2
    Temp3 = float(veri[9]) #in temp 3
    Temp4 = float(veri[10]) #in temp 4
    Temp5 = float(veri[11]) #in temp 5
    Temp6 = float(veri[12]) #in temp 6

    print(zaman, Host,Temp,Hum, AQI,Light,Motion,OutTemp, Amp, Temp2, Temp3, Temp4, Temp5, Temp6)
    global geldi
    g_zaman = int(zaman[4]+zaman[5])
    if g_zaman == geldi:
        print(">>>>> Çakışma var")
        pass
    else:
        guncellemeler(veri)
        print("       >>> veritabanı güncellendi\n")
    geldi = g_zaman
    client.disconnect()
    


def get_data(host):
    #print(geldi)
    try:
        #threading.Timer(10.0, partial(get_data, host)).start()
        subscribe.callback(on_message_print, "aks/veri", hostname=host)
    except:
        print(">>> "+host, " adress network error !\n")



def server():
    print(">>> Creating connections ..")
    time.sleep(0)
    print("..Done")
    
    new_ten = 0
    old_ten = 0
    
    while True:
        datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
        pilist=cPickle.load(datapi)
        datapi.close()
        time.sleep(0)

        if len(pilist)>0:
            for name,hostname in sorted(pilist.items()): 
                time.sleep(0)
                get_data(hostname)
                time.sleep(1)
        else:
            pass
            
        new_ten = int(time.strftime("%H%M%S")[2])
        
        if new_ten == old_ten:
            pass
        else:
            print("Web grafikleri güncellendi")
            try:

                os.system("scp -r "+os.getcwd()+"/../dems/results/images nkiwz8o19vf0@labaspire.com:/home/nkiwz8o19vf0/public_html/dems/results/")

            except:
            
                print("Web graphs could not up to date - Network connection error")
                pass
                
        old_ten = new_ten
        
            
        



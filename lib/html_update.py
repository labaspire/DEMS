#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time
from time import gmtime, strftime
import numpy as np
import os


def update_html(piname,hostname,pilist):

    # main.html dosyasini oku
    with open('main.html', 'r') as file:
        # Dosyadaki satirlari oku
        data = file.readlines()
        
    links = []    
    for x in data:
        link = find_between(x,'            <li class="probootstrap-animate" data-animate-effect="fadeInLeft"><a href="','.html')
        if link != "":
            links.append(link)
        else:
            pass
            
            
    # main.html icindeki eski linkleri sil        
    for link in links:
        try:
            data.remove('            <li class="probootstrap-animate" data-animate-effect="fadeInLeft"><a href="'+link+'.html">'+link+'</a></li>\n')
            
        except:
            pass        
            
            
    # main.html icine yeni linkleri ekle
    for piname,hostname in sorted(pilist.items()):
        newLocation = '            <li class="probootstrap-animate" data-animate-effect="fadeInLeft"><a href="'+piname+'.html">'+piname+'</a></li>\n'
        data.insert(40,newLocation)        
            


    # Yeni main.html dosyasini kaydet
    with open('main.html', 'w') as file:
        file.writelines( data )




# Lokasyonlar icin .html dosyalarini guncelle
def edit_html(piname,hostname):
    
    # Lokasyonun .html dosyasini oku 
    with open(piname+'.html', 'r') as file:
        # Dosyadaki satirlari oku
        data = file.readlines()
        
    # Dosya icindeki duzenlemeler
    #temperature
    
    

    

    data[59] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'günlükSıcaklık.png"><img src ="/results/images/'+hostname+'günlükSıcaklık.png" alt="Park"></a><div class="caption"><h3>Günlük Sıcaklık Grafiği</h3></div></div></div>\n'
    data[60] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'haftalikSıcaklık.png"><img src ="/results/images/'+hostname+'haftalikSıcaklık.png" alt="Park"></a><div class="caption"><h3>Haftalik Sıcaklık Grafiği</h3></div></div></div>\n'
    data[61] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'aylikSıcaklık.png"><img src ="/results/images/'+hostname+'aylikSıcaklık.png" alt="Park"></a><div class="caption"><h3>Aylik Sıcaklık Grafiği</h3></div></div></div>\n'
    data[62] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'yillikSıcaklık.png"><img src ="/results/images/'+hostname+'yillikSıcaklık.png" alt="Park"></a><div class="caption"><h3>Yıllık Sıcaklık Grafiği</h3></div></div></div>\n'   
    
    
    data[64] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'günlükNem.png"><img src ="/results/images/'+hostname+'günlükNem.png" alt="Park"></a><div class="caption"><h3>Günlük Nem Grafiği</h3></div></div></div>\n'
    data[65] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'haftalikNem.png"><img src ="/results/images/'+hostname+'haftalikNem.png" alt="Park"></a><div class="caption"><h3>Haftalık Nem Grafiği</h3></div></div></div>\n'
    data[66] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'aylikNem.png"><img src ="/results/images/'+hostname+'aylikNem.png" alt="Park"></a><div class="caption"><h3>Aylık Nem Grafiği</h3></div></div></div>\n'
    data[67] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'yillikNem.png"><img src ="/results/images/'+hostname+'yillikNem.png" alt="Park"></a><div class="caption"><h3>Yıllık Nem Grafiği</h3></div></div></div>\n'   
    
    data[69] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'günlükHava_Kalitesi.png"><img src ="/results/images/'+hostname+'günlükHava_Kalitesi.png" alt="Park"></a><div class="caption"><h3>Günlük Hava Kalitesi Grafiği</h3></div></div></div>\n'
    data[70] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'haftalikHava_Kalitesi.png"><img src ="/results/images/'+hostname+'haftalikHava_Kalitesi.png" alt="Park"></a><div class="caption"><h3>Haftalık Hava Kalitesi Grafiği</h3></div></div></div>\n'
    data[71] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'aylikHava_Kalitesi.png"><img src ="/results/images/'+hostname+'aylikHava_Kalitesi.png" alt="Park"></a><div class="caption"><h3>Aylık Hava Kalitesi Grafiği</h3></div></div></div>\n'
    data[72] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'yillikHava_Kalitesi.png"><img src ="/results/images/'+hostname+'yillikHava_Kalitesi.png" alt="Park"></a><div class="caption"><h3>Yıllık Hava Kalitesi Grafiği</h3></div></div></div>\n'       

    data[74] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'günlükIşık_Şiddeti.png"><img src ="/results/images/'+hostname+'günlükIşık_Şiddeti.png" alt="Park"></a><div class="caption"><h3>Günlük Işık Şiddeti Grafiği</h3></div></div></div>\n'
    data[75] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'haftalikIşık_Şiddeti.png"><img src ="/results/images/'+hostname+'haftalikIşık_Şiddeti.png" alt="Park"></a><div class="caption"><h3>Haftalık Işık Şiddeti Grafiği</h3></div></div></div>\n'
    data[76] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'aylikIşık_Şiddeti.png"><img src ="/results/images/'+hostname+'aylikIşık_Şiddeti.png" alt="Park"></a><div class="caption"><h3>Aylık Işık Şiddeti Grafiği</h3></div></div></div>\n'
    data[77] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'yillikIşık_Şiddeti.png"><img src ="/results/images/'+hostname+'yillikIşık_Şiddeti.png" alt="Park"></a><div class="caption"><h3>Yıllık Işık Şiddeti Grafiği</h3></div></div></div>\n'       
    

    data[79] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'günlükEnerji.png"><img src ="/results/images/'+hostname+'günlükEnerji.png" alt="Park"></a><div class="caption"><h3>Günlük Enerji Grafiği</h3></div></div></div>\n'
    data[80] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'haftalikEnerji.png"><img src ="/results/images/'+hostname+'haftalikEnerji.png" alt="Park"></a><div class="caption"><h3>Haftalık Enerji Grafiği</h3></div></div></div>\n'
    data[81] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'aylikEnerji.png"><img src ="/results/images/'+hostname+'aylikEnerji.png" alt="Park"></a><div class="caption"><h3>Aylık Enerji Grafiği</h3></div></div></div>\n'
    data[82] ='        <div class="col-sm-6 col-md-4"><div class="thumbnail"><a class="lightbox" href="/results/images/'+hostname+'yillikEnerji.png"><img src ="/results/images/'+hostname+'yillikEnerji.png" alt="Park"></a><div class="caption"><h3>Yıllık Enerji Grafiği</h3></div></div></div>\n'   


    # Yeni .html dosyasini kaydet
    with open(piname+'.html', 'w') as file:
        file.writelines( data )
    


def edit_emergency(name,etype):
    
    with open(os.getcwd()+'/../dems/emergency.html', 'r') as file:
        
        # Dosyadaki satirlari oku
        data = file.readlines()
        
    emergency_places = []
        
    for x in data:
        emergency_place = find_between(x,'  </tr><tr><td>','</td></tr>')
        if emergency_place != "":
            emergency_places.append(emergency_place)
            
        else:
            pass
            
    nof_emergency = len(emergency_places)
    
    table_first = 81
    
    now = strftime("%Y-%m-%d %H:%M:%S", gmtime())

    new_emergency ="   </tr><tr><td>"+name+"</td><td>"+etype+"</td><td>"+now+"</td></tr>\n"
    
    data.insert(table_first,new_emergency)
    
    with open(os.getcwd()+'/../dems/emergency.html', 'w') as file:
        file.writelines( data )
        
def del_emergency():

    with open(os.getcwd()+'/../dems/emergency.html', 'r') as file:
        
        # Dosyadaki satirlari oku
        data = file.readlines()
        
    emergency_places = []
    deleteList = []   
    for x in data:
        emergency_place = find_between(x,'   </tr><tr><td>','</td></tr>')
        if emergency_place != "":
            #emergency_places.append(emergency_place)
            deleteList.append(x)
        else:
            pass
            
    for x in deleteList:
        data.remove(x)
            
            
    #print(emergency_places)
    with open(os.getcwd()+'/../dems/emergency.html', 'w') as file:
        file.writelines( data )
    


# Bir str dizide belirli indexleri bulan fonksiyon
def find_between( s, first, last ):

    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""
    


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import *
from PyQt5.QtCore import * 
from PyQt5.QtWidgets import QInputDialog, QLineEdit, QDialog
from PyQt5 import QtCore, QtGui, QtWidgets
import os 
import pickle as cPickle
import sys 
from functools import partial 
import rrdtool    
from veri_penceresi import *

class alan_ekran(QDialog):
    def __init__(self, parent=None):
        super(alan_ekran, self).__init__(parent) 
        
        selected = open(os.getcwd()+"/../data/secili_alan.db", 'rb')
        secili_alan=cPickle.load(selected)
        selected.close() 
        for self.piname,self.hostname in sorted(secili_alan.items()):
            #print (self.piname,self.hostname)
            pass

        #burada rrd dosyasının son verisi 
        sonVeriler=rrdtool.lastupdate(os.getcwd()+"/../data/"+self.hostname+".rrd")
        sonGuncellemezamani=sonVeriler['date']
        sonSensorVerisi=sonVeriler['ds']
        
        sicaklik=sonVeriler['ds']['sicaklik']
        nemi=sonVeriler['ds']['nemorani']
        isiki=sonVeriler['ds']['isik']
        havak=sonVeriler['ds']['havakalitesi']
        isiki = str(isiki)
        isiki = isiki[0]+isiki[1]+isiki[2]+isiki[3]+isiki[4]+isiki[5]+isiki[6]

        font_kont = QtGui.QFont("TimesNewRoman",9,QtGui.QFont.Bold,False)

        self.win_kon = QtWidgets.QGridLayout()

        self.anlik_baslik=QtWidgets.QLabel(self.piname+" Anlik Değerleri")
        self.anlik_baslik.setStyleSheet('color: teal')
        self.anlik_baslik.setFont(font_kont)
        self.anlik_baslik.setAlignment(Qt.AlignCenter)
        self.win_kon.addWidget(self.anlik_baslik, 0, 1, 1, 2)

        self.ortam_veri=QtWidgets.QLabel("Sıcaklık:"+str(sicaklik)+" C "+"  Nem Oranı:"+str(nemi)+"  Işık Miktarı:"+str(isiki)+"  Hava Kalitesi:"+str(havak))
        self.ortam_veri.setStyleSheet('color: dimgray')
        self.ortam_veri.setFont(font_kont)
        self.ortam_veri.setAlignment(Qt.AlignCenter)
        self.win_kon.addWidget(self.ortam_veri, 1, 1, 1, 2)    

        self.tarihi=QtWidgets.QLabel("Son Güncelleme Zamanı:"+str(sonGuncellemezamani))   
        self.tarihi.setStyleSheet('color: dimgray')
        self.tarihi.setFont(font_kont) 
        self.tarihi.setAlignment(Qt.AlignCenter)
        self.win_kon.addWidget(self.tarihi, 2, 1, 1, 2) 

        self.grafik_baslik=QtWidgets.QLabel(self.piname+" Kontrol Grafikleri (Günlük  Haftalık  Aylık Yıllık)")
        self.grafik_baslik.setStyleSheet('color: teal')
        self.grafik_baslik.setFont(font_kont) 
        self.grafik_baslik.setAlignment(Qt.AlignCenter)
        self.win_kon.addWidget(self.grafik_baslik, 3, 1, 1, 2)


        self.combog = QtWidgets.QComboBox()
        self.combog.addItems(["Görmek İstediğiniz Grafik Türünü Seçiniz","Sıcaklık", "Nem", "Hava_Kalitesi","Işık_Şiddeti","Enerji"])
        self.combog.setStyleSheet('color: dimgray')
        self.combog.setFont(font_kont) 
        self.win_kon.addWidget(self.combog, 5, 1, 1, 2)


        self.secileng=self.combog.currentText()
        self.combog.activated.connect(self.grafik_goster)

        self.konfor = QtWidgets.QLabel()
        self.konfor.setPixmap(QPixmap(os.getcwd()+"/../dems/results/images/"+self.hostname+"konfor_bolgesi.png"))  
        self.win_kon.addWidget(self.konfor, 6, 0, 1, 2)
        
        self.room = QtWidgets.QLabel()
        self.room.setPixmap(QPixmap(os.getcwd()+"/../dems/results/images/"+self.hostname+"temperatures.png"))  
        self.win_kon.addWidget(self.room, 6, 2, 1, 2)


        self.setLayout(self.win_kon)       
        #self.win_kon.setGeometry(750,750,520,700)
        self.setWindowTitle(self.piname+" Kontrol Penceresi")
        self.setMinimumSize(1040,700)
        self.resize(1040,600)
        self.show()

    def grafik_goster(self):
        self.secileng=self.combog.currentText()
        secileng = self.secileng
        hostname = self.hostname
        self.dialog=grafik_penceresi(self, secileng, hostname)
        self.dialog.show()








    
        

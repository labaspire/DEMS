#!/usr/bin/env python3
import os,sys
from configparser import SafeConfigParser
from grafik_olustur import *
from konfor_grafik import *
from roomtemps import *
from akim_guncelle import *
import pickle as cPickle
from alarm import *
from html_update import *


def guncellemeler(veri):

    #--------------------------------    
    #Read config file for alert limits
    config = SafeConfigParser()
    config.read("../conf/dems.cfg")
    config.sections()
    #--------------------------------





    #--------------------------------
    Host = veri[0]
    Temp = float(veri[1]) #intemp1
    Hum = float(veri[2])
    AQI = float(veri[3])
    Light = float(veri[4])
    Motion = float(veri[5])
    OutTemp = float(veri[6])
    Amp = float(veri[7])    
    Temp2 = float(veri[8]) #in temp 2
    Temp3 = float(veri[9]) #in temp 3
    Temp4 = float(veri[10]) #in temp 4
    Temp5 = float(veri[11]) #in temp 5
    Temp6 = float(veri[12]) #in temp 6
    #--------------------------------
    
    
    
    
    
    #--------------------------------
    if Amp > 0.4:
        Amp = 1
    else:
        pass
        
    if Amp < 0.5:
        Amp = 0
    else:
        pass
    #--------------------------------
    
    
    
    
    #--------------------------------
    lux = (1.25*(10**7))*(Light**-1.4059)
    #print(lux)
    Light = float(lux)
    #--------------------------------
    
    
    
    
    
    #--------------------------------
    datapi = open(os.getcwd()+"/../data/veritabani.db", 'rb')
    pilist=cPickle.load(datapi)
    datapi.close()    

    for piname,hostname in sorted(pilist.items()):
        if hostname == Host:
            name = piname
        else:
            name = "unknown"
    #--------------------------------
    
    
    
    
    

    #--------------------------------
    AlertPlaces = []

    try :
        MinTemp = int(config.get(name,"MinTemp"))
        
    except :
        MinTemp = int(config.get("DEFAULT","MinTemp"))
        
    try :
        MaxTemp = int(config.get(name,"MaxTemp"))
        #print(MinTemp)
    except :
        MaxTemp = int(config.get("DEFAULT","MaxTemp"))                

    try :
        MinHum = int(config.get(name,"MinHum"))
        #print(MinTemp)
    except :
        MinHum = int(config.get("DEFAULT","MinHum"))            

    try :
        MaxHum = int(config.get(name,"MaxHum"))
        #print(MinTemp)
    except :
        MaxHum = int(config.get("DEFAULT","MaxHum"))            

    try :
        MaxAQI = int(config.get(name,"MaxAQI"))
        #print(MinTemp)
    except :
        MaxAQI = int(config.get("DEFAULT","MaxAQI"))
        
    
    if AQI > MaxAQI:
        try:
            acil_durum_ses()
        except ValueError and NameError and TypeError:
            pass
            
        edit_emergency(name,"Air Quality")
    else:
        pass


        
    if Temp < MinTemp or Temp > MaxTemp:
        edit_emergency(name,"Temperature")
    else:
        pass     
        
        
    if Hum < MinHum or  Hum > MaxHum:
        edit_emergency(name,"Humidity")
    else:
        pass
    #--------------------------------

        
        
        


    #--------------------------------
    dosyaAdi=Host+".rrd"
    dosyaKonumu=os.getcwd()+"/../data/"+dosyaAdi
      
        
    enerji_dosyaAdi=Host+"enerji.rrd"
    enerji_dosyaKonumu=os.getcwd()+"/../data/"+enerji_dosyaAdi      
    #--------------------------------
    
    
    
    
    
    
    
    #--------------------------------  
    if os.path.isfile(dosyaKonumu):
        pass
        #print( dosyaAdi,"icin veritabani daha once olusturulmus.")
    else:
        veritabani(dosyaKonumu)
    veritabaniGuncelle(dosyaKonumu,Temp,Light,AQI,Hum,OutTemp,dosyaAdi,Temp2,Temp3,Temp4,Temp5,Temp6)
    grafikCiz(Host,dosyaKonumu)
  
    
    if os.path.isfile(enerji_dosyaKonumu):
        pass
        #print( dosyaAdi,"icin veritabani daha once olusturulmus.")
    else:
        TuketimHareketVeritabani(enerji_dosyaKonumu)
    TuketimHareketVeritabaniGuncelle(enerji_dosyaKonumu,Amp,Motion,enerji_dosyaAdi)
    TuketimHareketGrafik(Host,enerji_dosyaKonumu)      
    #--------------------------------

    
    
    
    
    
    #--------------------------------
    try :
        gr_konfor(Host,Temp,Hum)
    except:
        pass
        
    try :
        roomtemps_graph(Host,Temp,Temp2,Temp3,Temp4,Temp5,Temp6)
    except:
        pass
    #--------------------------------    
        

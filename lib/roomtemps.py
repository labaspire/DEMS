#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d.axis3d import Axis



def roomtemps_graph(host,t1,t2,t3,t4,t5,t6):

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    
    
    #--------------------------------------------------
    if not hasattr(Axis, "_get_coord_info_old"):
        def _get_coord_info_new(self, renderer):
            mins, maxs, centers, deltas, tc, highs = self._get_coord_info_old(renderer)
            mins += deltas / 4
            maxs -= deltas / 4
            return mins, maxs, centers, deltas, tc, highs
        Axis._get_coord_info_old = Axis._get_coord_info  
        Axis._get_coord_info = _get_coord_info_new
    #--------------------------------------------------





    #--------------------------------------------------
    lw=1
    linest="--"
    text_color="black"
    line_color = "black"
    text_font = 10
    f_name="Arial"
    mkr="o"
    mkr_size=6
    
    Temp_list = [t1,t2,t3,t4,t5,t6]
    x_list = [0,0,0.5,1,0.5,0]
    y_list = [1,0.5,1,0.5,0,1]
    z_list = [0,0.5,0.5,0.5,0.5,1]
    
    ortalama = np.mean(Temp_list)
    ortalama = str(ortalama)
    ortalama = ortalama[0]+ortalama[1]+ortalama[2]+ortalama[3]
    #print(ortalama)
    #--------------------------------------------------


    #--------------------------------------------------
    for val in np.arange(0,len(x_list)):


        z_vals = np.linspace(0, z_list[val], 3)
        x_vals =[x_list[val],x_list[val],x_list[val]]
        y_vals = [y_list[val],y_list[val],y_list[val]]

        ax.plot(x_vals, y_vals, z_vals, label='', color = line_color, linestyle=linest,lw=lw)
        ax.plot([x_list[val]],[y_list[val]],[z_list[val]], marker=mkr,  markersize=mkr_size)
        ax.text(x_list[val],y_list[val],z_list[val], ("  "+str(Temp_list[val])+"C°"), color=text_color ,fontname=f_name, fontsize=text_font)
        
    l1 = plt.scatter([],[], s=0, edgecolors='gold', facecolors = 'gold')
    leg = plt.legend([l1], "" , ncol=2, frameon=True, fontsize=5, handlelength=2, loc = 1, borderpad = 1, handletextpad=0, title='Ortalama\n'+ortalama+'40C°', scatterpoints = 1)
    leg.get_frame().set_facecolor('#9C9C9C')


    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    ax.set_zlim(0, 1)

    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels(["Zemin","","","","","Tavan"], fontname="Arial", fontsize=8)

    ax.set_xlabel('Kuzey',fontname="Arial", fontsize=8)
    ax.set_ylabel('Batı',fontname="Arial", fontsize=8)

    ax.view_init(elev=42, azim=-41)

    plt.title("Birim Sıcaklıkları                                         ",fontname="Arial", fontsize=12)

    plt.savefig(os.getcwd()+"/../dems/results/images/"+host+"temperatures.png",transparent = True, bbox_inches='tight', pad_inches=0)
    #--------------------------------------------------





#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from PyQt5 import QtCore, QtGui
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QInputDialog, QLineEdit, QDialog
from PyQt5 import QtCore, QtGui, QtWidgets
import os
import pickle as cPickle
import sys
from kontrol_ekrani import *

#Son aşamada bütün pencereler için ayrı class yazarak tekrar eden birçok hata giderildi.

class grafik_penceresi(QDialog):
    def __init__(self, str, secileng, hostname):
        super(grafik_penceresi, self).__init__()
        self.gr_win = QtWidgets.QGridLayout()
        #self.setCentralWidget(gr_win)
        #print(hostname, secileng)

        self.gunluk = QtWidgets.QLabel()
        self.gunluk.setPixmap(QPixmap(os.getcwd()+"/../dems/results/images/"+hostname+"günlük"+secileng+".png"))
        self.gr_win.addWidget(self.gunluk, 0, 0)

        self.haftalik = QtWidgets.QLabel()
        self.haftalik.setPixmap(QPixmap(os.getcwd()+"/../dems/results/images/"+hostname+"haftalik"+secileng+".png"))
        self.gr_win.addWidget(self.haftalik, 0, 1
)
        self.aylik = QtWidgets.QLabel()
        self.aylik.setPixmap(QPixmap(os.getcwd()+"/../dems/results/images/"+hostname+"aylik"+secileng+".png"))
        self.gr_win.addWidget(self.aylik, 1, 0)

        self.yillik = QtWidgets.QLabel()
        self.yillik.setPixmap(QPixmap(os.getcwd()+"/../dems/results/images/"+hostname+"yillik"+secileng+".png"))
        self.gr_win.addWidget(self.yillik, 1, 1)

        self.setLayout(self.gr_win)
        self.setWindowTitle(" Grafik Gözlem Penceresi")
        self.setMinimumSize(1360,720)
        self.show()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import rrdtool
from rrdtool import update as rrd_update
import time
import datetime
import os
def grafikOlustur():
    """while True:
        piids={'merkez1':1,'merkez2':2,'merkez3':3}
        piips={'pi1':1,'pi2':2,'pi3':3} 
        sicaklik=10
        isik=20
        havakalitesi=30
        nemorani=40
        for piid in sorted(piids.items()):
            dosyaAdi=str(piid)+".rrd"
            dosyaKonumu=os.getcwd()+"/../data/"+dosyaAdi
            if os.path.isfile(dosyaKonumu):
                print( dosyaAdi,"Icin veritabani daha once olusturulmus.")
            else:
                veritabani(dosyaKonumu)
        for piid in sorted(piids.items()):
            dosyaAdi=str(piid)+".rrd"
            dosyaKonumu=os.getcwd()+"/../data/"+dosyaAdi
            veritabaniGuncelle(dosyaKonumu,sicaklik,isik,havakalitesi,nemorani,dosyaAdi)
            grafikCiz(piid,dosyaKonumu)
    return True"""
def veritabani(dosyaKonumu):                  #sensorlerden alinan veriler 
                                              #icin rrd veritabani olusturuldu
                                              #her 5 dakikada bir sensorlerden veri al
                                              #1 gunde 5 dakika conuzurluk
                                              #1 haftada 15 dakika cozunurluk
                                              #1 ayda 1 saat cozunurluk
                                              #1 yilda 6 saat cozunurluk
    veritabani=rrdtool.create(dosyaKonumu,
    "--step","300","--start",'0',
    "DS:sicaklik:GAUGE:600:0:70",
    "DS:isik:GAUGE:600:0:15000",
    "DS:havakalitesi:GAUGE:600:0:500",
    "DS:nemorani:GAUGE:600:-5:105",
    "DS:dis_sicaklik:GAUGE:600:-20:60",
    "DS:Temp2:GAUGE:600:0:70",
    "DS:Temp3:GAUGE:600:0:70",
    "DS:Temp4:GAUGE:600:0:70",
    "DS:Temp5:GAUGE:600:0:70",
    "DS:Temp6:GAUGE:600:0:70",
    "RRA:AVERAGE:0.5:1:288",
    "RRA:AVERAGE:0.5:6:672",
    "RRA:AVERAGE:0.5:24:744",
    "RRA:AVERAGE:0.5:288:1480",
    "RRA:MAX:0.5:1:288",
    "RRA:MAX:0.5:6:672",
    "RRA:MAX:0.5:24:744",
    "RRA:MAX:0.5:288:1480",
    "RRA:MIN:0.5:1:288",
    "RRA:MIN:0.5:6:672",
    "RRA:MIN:0.5:24:744",
    "RRA:MIN:0.5:288:1480")
    #print(dosyaKonumu, "Sensor degerleri icin veritabani olusturuldu.")
    return veritabani
    
    
    
    
def veritabaniGuncelle(dosyaKonumu,sicaklik,isik,havakalitesi,nemorani,dis_sicaklik,dosyaAdi,Temp2,Temp3,Temp4,Temp5,Temp6):
    rrd_update(dosyaKonumu,'N:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s'%(sicaklik,isik,havakalitesi,nemorani,dis_sicaklik,Temp2,Temp3,Temp4,Temp5,Temp6));
    #print(dosyaAdi, "icin veritabanlari guncellendi.")
    
    
    
def grafikCiz(piid,dosyaKonumu):
    piid=str(piid)
    sonGuncelleme = rrdtool.last(dosyaKonumu)
    sonGuncellemedf = datetime.datetime.fromtimestamp(int(sonGuncelleme)).strftime('%Y-%m-%d %H\:%M\:%S')
    for z in ["daily","weekly","monthly","yearly"]:
        if z== "daily":
            period="d"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"günlükSıcaklık.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=230",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "DEF:Temp2="+dosyaKonumu+":Temp2:AVERAGE",
            "DEF:Temp3="+dosyaKonumu+":Temp3:AVERAGE",
            "DEF:Temp4="+dosyaKonumu+":Temp4:AVERAGE",
            "DEF:Temp5="+dosyaKonumu+":Temp5:AVERAGE",
            "DEF:Temp6="+dosyaKonumu+":Temp6:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi 1",
            "LINE1:Temp2#FFA22B:Oda Sicakligi 2",
            "LINE1:Temp3#F2FF2B:Oda Sicakligi 3",
            "LINE1:Temp4#2BFF72:Oda Sicakligi 4",
            "LINE1:Temp5#2BC5FF:Oda Sicakligi 5",
            "LINE1:Temp6#5B2BFF:Oda Sicakligi 6",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"günlükIşık_Şiddeti.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=245",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"günlükNem.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=245",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"günlükHava_Kalitesi.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=245",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
        if z== "weekly":
            period="w"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"haftalikSıcaklık.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=230",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "DEF:Temp2="+dosyaKonumu+":Temp2:AVERAGE",
            "DEF:Temp3="+dosyaKonumu+":Temp3:AVERAGE",
            "DEF:Temp4="+dosyaKonumu+":Temp4:AVERAGE",
            "DEF:Temp5="+dosyaKonumu+":Temp5:AVERAGE",
            "DEF:Temp6="+dosyaKonumu+":Temp6:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi 1",
            "LINE1:Temp2#FFA22B:Oda Sicakligi 2",
            "LINE1:Temp3#F2FF2B:Oda Sicakligi 3",
            "LINE1:Temp4#2BFF72:Oda Sicakligi 4",
            "LINE1:Temp5#2BC5FF:Oda Sicakligi 5",
            "LINE1:Temp6#5B2BFF:Oda Sicakligi 6",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",             
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"haftalikIşık_Şiddeti.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=245",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"haftalikNem.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=245",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"haftalikHava_Kalitesi.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=245",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
        if z== "monthly":
            period="m"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"aylikSıcaklık.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=230",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "DEF:Temp2="+dosyaKonumu+":Temp2:AVERAGE",
            "DEF:Temp3="+dosyaKonumu+":Temp3:AVERAGE",
            "DEF:Temp4="+dosyaKonumu+":Temp4:AVERAGE",
            "DEF:Temp5="+dosyaKonumu+":Temp5:AVERAGE",
            "DEF:Temp6="+dosyaKonumu+":Temp6:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi 1",
            "LINE1:Temp2#FFA22B:Oda Sicakligi 2",
            "LINE1:Temp3#F2FF2B:Oda Sicakligi 3",
            "LINE1:Temp4#2BFF72:Oda Sicakligi 4",
            "LINE1:Temp5#2BC5FF:Oda Sicakligi 5",
            "LINE1:Temp6#5B2BFF:Oda Sicakligi 6",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"aylikIşık_Şiddeti.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=245",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"aylikNem.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=245",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"aylikHava_Kalitesi.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=245",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
        if z== "yearly":
            period="y"
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"yillikSıcaklık.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Sicaklik(C)", "--height=230",
            "DEF:sicaklik="+dosyaKonumu+":sicaklik:AVERAGE",
            "DEF:dis_sicaklik="+dosyaKonumu+":dis_sicaklik:AVERAGE",
            "DEF:Temp2="+dosyaKonumu+":Temp2:AVERAGE",
            "DEF:Temp3="+dosyaKonumu+":Temp3:AVERAGE",
            "DEF:Temp4="+dosyaKonumu+":Temp4:AVERAGE",
            "DEF:Temp5="+dosyaKonumu+":Temp5:AVERAGE",
            "DEF:Temp6="+dosyaKonumu+":Temp6:AVERAGE",
            "LINE1:sicaklik#00FF00:Oda Sicakligi 1",
            "LINE1:Temp2#FFA22B:Oda Sicakligi 2",
            "LINE1:Temp3#F2FF2B:Oda Sicakligi 3",
            "LINE1:Temp4#2BFF72:Oda Sicakligi 4",
            "LINE1:Temp5#2BC5FF:Oda Sicakligi 5",
            "LINE1:Temp6#5B2BFF:Oda Sicakligi 6",
            "LINE1:dis_sicaklik#FF0000:Dis Sicaklik",
            "COMMENT:\\n",
            "GPRINT:sicaklik:MIN:Min Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:sicaklik:MAX:Max Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",
            "GPRINT:dis_sicaklik:MIN:Min Dis Sicaklik\: %6.2lfC",
            "COMMENT:  ",
            "GPRINT:dis_sicaklik:MAX:Max Dis Sicaklik\: %6.2lfC\\r",                
            "COMMENT:\\n",             
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"yillikIşık_Şiddeti.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Isik Siddeti", "--height=245",
            "DEF:isik="+dosyaKonumu+":isik:AVERAGE",
            "LINE1:isik#FF0000:Isik Seviyesi",
            "GPRINT:isik:MIN:Min Isik\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:isik:MAX:Max Isik\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"yillikNem.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Nem(%)", "--height=245",
            "DEF:nemorani="+dosyaKonumu+":nemorani:AVERAGE",
            "LINE1:nemorani#000000:Nem Orani\\r",
            "GPRINT:nemorani:MIN:Min Nem Orani\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:nemorani:MAX:Max Nem Orani\: %6.2lf\\r",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)
            ciz=rrdtool.graph(os.getcwd()+"/../dems/results/images/"+piid+"yillikHava_Kalitesi.png","--start","-1%s"%(period),"--width=575", 
            "--title="+piid+" Kontrol Monitoru","--watermark=ASPIRE LAB", "--vertical-label=Hava Kalitesi", "--height=245",
            "DEF:havakalitesi="+dosyaKonumu+":havakalitesi:AVERAGE",
            "LINE1:havakalitesi#0000FF:Hava Kalitesi",
            "GPRINT:havakalitesi:MIN:Min Hava Kalitesi\: %6.2lf",
            "COMMENT:  ",
            "GPRINT:havakalitesi:MAX:Max Hava Kalitesi\: %6.2lf\\r",
            "COMMENT:\\n",              
            "COMMENT: Son guncelleme\: %s" %sonGuncellemedf)




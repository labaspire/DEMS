#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
                                ██╗   ██╗███╗   ██╗██╗████████╗
                                ██║   ██║████╗  ██║██║╚══██╔══╝
                                ██║   ██║██╔██╗ ██║██║   ██║   
                                ██║   ██║██║╚██╗██║██║   ██║   
                                ╚██████╔╝██║ ╚████║██║   ██║   
                                 ╚═════╝ ╚═╝  ╚═══╝╚═╝   ╚═╝   

"""                               
import numpy as np
import os,sys
from multiprocessing import Process
os.chdir("../")
sys.path.append('lib')
from AQIsense import *
from InfiniteCurrent import *
from LightSense import *
from MotionSense import *
from DataSender import *
from OutsideTemp import *
from RelayControl import *
from TempHum import *
from NetworkCheck import *
os.chdir("bin")


MotionList = []

LightList = []



def readAllData():

    DataList = []

    DataList.append("95.183.170.225")

    Temperature,Humidity = readTempHum()
    DataList.append(Temperature)
    DataList.append(Humidity)

    AQI = readAQI()
    DataList.append(AQI)

    light = readLight()
    DataList.append(light)

    motion = readMotion()
    DataList.append(motion)

    outTemp = readOutTemp()
    DataList.append(outTemp)
        
    current = readCurrent()
    DataList.append(current)
    
    EnergyDataGeneration(light,motion,current)
    
      
    print(DataList)    
    return DataList





def NetworkChecks():
    
    CheckNet()
    



    
def sendData():
    
    while True:
        
    
        DataList = readAllData()
        
        sendCenter(str(DataList))
   
        time.sleep(20)






def EnergyDataGeneration(light,motion,current):


    if len(MotionList) > 14:
        
        # Remove oldest data
        MotionList.remove(MotionList[0])
        
        # Add newest data
        MotionList.append(motion)
        
    else:
        
        MotionList.append(motion)
    
    
    
    if len(LightList) > 14:
        
        # Remove oldest data
        LightList.remove(LightList[0])
        
        # Add newest data
        LightList.append(light)
        
    else:
        
        LightList.append(light)
        
    DataInterpreter(current,MotionList,LightList)    





      
def DataInterpreter(current,MotionList,LightList):

    user = False
    
    lightNeed = False
    
    sunLight = False
    
    lamp = False
    
    
    print("User activty list : ",MotionList)
    
    for motion in MotionList:
    
        if motion == 1:
            user = True
            
        elif motion == 0:
            pass
                
        else:
            pass
            


    
    light = np.mean(LightList)
    
    if light > 700:        
        lightNeed = True
        
    elif light < 500:    
        sunLight = True
        
    else:
        pass
        
        
        
    
    if current == 0:
        lamp = False
        
    else:
        lamp = True
        
        

    EnergyDemand = False
    """
    if user == False:
    
        if lamp == True:
            
            EnergyDemand = False
            
        else:
            
            pass

        
    elif user == True:
    
        if lamp == True:
            
            if lightNeed == True:
            
                EnergyDemand = True
                
            else:
            
                EnergyDemand = False
                
        elif lamp == False:
        
            if lightNeed == True:
            
                EnergyDemand = True
                
            elif lightNeed == False:
            
                pass"""
                
                
    if user == False:
    
        if lamp == True:
            
            EnergyDemand = False
            
        else:
            
            pass

        
    elif user == True:
    
        if lamp == True:
            
            EnergyDemand = True
                
        elif lamp == False:
        
            EnergyDemand = True
                
    EnergySaver(EnergyDemand)   
            
                     
    
        
                
        
def EnergySaver(EnergyDemand):

    print( "Enerji talebi : ",EnergyDemand)

    current = readCurrent()
        
    if current == 0:
        lamp = False
        
    else:
        lamp = True
        
        
        
    if lamp == True and EnergyDemand == True:
        
        pass
        
        
        
            
    elif lamp == True and EnergyDemand == False:
    
        relays("off")
        
        time.sleep(1)
        
        current = readCurrent()
        
        if current == 0:
            newlamp = False
            
        else:
            newlamp = True   
            
        if newlamp == False:
        
            pass
            
        elif newlamp == True:
        
            relays("on")
            
        else:
            
            pass
                             


        
    elif lamp == False and EnergyDemand == True:
    
        pass



        
    elif lamp == False and EnergyDemand == False:
    
        pass   



        
    else:
    
        pass
    
    

    """if EnergyDemand == True:
    
        #RelayOn
        relays(EnergyDemand)
        
        current = readCurrent()
        
        if current > 0:
        
            pass
            
        else:
            
            relays("off")

        
    elif EnergyDemand == False:
    
        #RelayOff
        relays("off")
        
        current = readCurrent()
        
        if current > 0:
        
            relays("on")
            
        else:
            
            pass
        
    else:
        pass"""
    
    



if __name__ == "__main__":

    DataSend = Process(target=sendData)
    DataSend.start()
    
    NetworkStat = Process(target=NetworkChecks)
    NetworkStat.start()


    
    

    
    


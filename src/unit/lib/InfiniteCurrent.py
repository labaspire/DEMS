#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import time
import os
import math
import spidev



# Open SPI bus
spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=488000

# Function to read SPI data from MCP3008 chip
# Channel must be an integer 0-7
def ReadChannel(channel):
    adc = spi.xfer2([1,(8+channel)<<4,0])
    data = ((adc[1]&3) << 8) + adc[2]
    return data

def readCurrent():

    amps = []
    #pos_amps=[]
    for i in np.arange(0,200):
        # Define sensor channels
        amp_channel = 0

        amp_level = ReadChannel(amp_channel)
        #amp_level = 525
        amp = float((amp_level-512)*5000)/1024/66/math.sqrt(2)
        #print(amp)

        amps.append(amp)

 
    positive_amps = np.abs(amps)
    max_amp = np.max(amps)   
    maxpos_amp = np.max(positive_amps)

    if max_amp > 0.50 or maxpos_amp > 0.35:
        durum = "acik"
    else :
        durum = "kapali" 
    

    #print(durum, "   #     Maxpos Amp :", maxpos_amp , "   #   Max Amp :", max_amp)
    
    

    if durum == "acik":
        amplitude = float(maxpos_amp)-0.05
    else :
        amplitude = 0
    
    return amplitude
            
        

      
        






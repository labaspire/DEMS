#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time
import os
import spidev
# Open SPI bus
spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=200

# Function to read SPI data from MCP3008 chip
# Channel must be an integer 0-7
def ReadChannel(channel):

    adc = spi.xfer2([1,(8+channel)<<4,0])
    data = ((adc[1]&3) << 8) + adc[2]
    return data


def readAQI():

    air_channel  = 1
    AQI = ReadChannel(air_channel)
    return AQI

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
import spidev
import time
 
spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz=200
 
def readadc(adcnum):

    if adcnum > 7 or adcnum < 0:
        return -1
    r   = spi.xfer2([1,(8+adcnum)<<4,0])
    adcout = ((r[1] & 3)<<8) + r[2]
    return adcout
 
def readOutTemp():

    value = readadc(3)
    volts = (value*5) / 1024
    temperature = volts / (10.0 / 1000)
    #print (temperature)
    return temperature


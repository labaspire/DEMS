#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import time
import gpiozero


RELAY_PIN = 27

relay = gpiozero.OutputDevice(RELAY_PIN, active_high=False, initial_value=False)


def set_relay(status):
    if status:
        print("Setting relay: ON")
        relay.on()
    else:
        print("Setting relay: OFF")
        relay.off()


def toggle_relay():
    print("toggling relay")
    relay.toggle()

	


def relays(islem):
    if islem == "on":
        set_relay(True)
    elif islem == "off":
        set_relay(False)
    else :
        pass
        
        
        
        

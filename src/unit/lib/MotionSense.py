#!/usr/bin/python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
def motionNow():
    pin=4 
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin,GPIO.IN)
    motion = GPIO.input(pin)
    return motion
    

def readMotion():
    motions = motionNow()
    #time.sleep(1)
    return motions

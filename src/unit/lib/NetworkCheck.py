#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO 
import time
import os



GPIO.setwarnings(False) 

GPIO.setmode(GPIO.BCM) 

pin = 21

GPIO.setup(pin, GPIO.OUT, initial=GPIO.LOW) 


    
def blinkit():

    
    GPIO.output(pin, GPIO.HIGH) 
    
    time.sleep(1) 
    
    GPIO.output(pin, GPIO.LOW) 
    
    time.sleep(1)
    
    GPIO.output(pin, GPIO.LOW)

    
    
def CheckNet():
    
    while True:
    
        host = "95.183.170.225"
        
        ipadress = os.popen('hostname -I') 
        ip = ipadress.read().split(' ')[0]
        
        if ip == host:
        
            blinkit()
            
        else:
            
            pass
            

